const teacher = [
{
    "teacher_id": 1,
    "first_name": "Theodora",
    "last_name": "Bennike",
    "Qualification": "B.ed",
    "date_join": "4/14/2007",
    "assign_subject":"Physics"
  }, {
    "teacher_id": 2,
    "first_name": "Maurizio",
    "last_name": "Pocknell",
    "Qualification": "B.A",
    "date_join": "5/5/2004",
    "assign_subject":"English"
  }, {
    "teacher_id": 3,
    "first_name": "Isahella",
    "last_name": "Flacke",
    "Qualification": "Msc",
    "date_join": "12/19/2015",
    "assign_subject":"Bio"
  },  {
    "teacher_id": 4,
    "first_name": "Melodee",
    "last_name": "Grahame",
    "Qualification": "Bsc(chemistry)",
    "date_join": "11/29/1989",
    "assign_subject":"Chem"
  }, {
    "teacher_id": 5,
    "first_name": "Larine",
    "last_name": "Middup",
    "Qualification": "Msc(Mathematics)",
    "date_join": "1/14/2006",
    "assign_subject":"Maths"
  },
  {
    "teacher_id": 6,
    "first_name": "Sayali",
    "last_name": "khadse",
    "Qualification": "B.ed",
    "date_join": "4/14/2007",
    "assign_subject":"Physics"
  },
  {
    "teacher_id": 7,
    "first_name": "Pankaj",
    "last_name": "khadse",
    "Qualification": "Msc",
    "date_join": "4/14/1993",
    "assign_subject":"chem"
  },]
;
  
  export default teacher;