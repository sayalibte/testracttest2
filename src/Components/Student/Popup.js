import React, { useRef, useEffect, useState } from "react";
import "./style.scss";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
function Popup({isModalOpen, closeModalPopup ,data}) {
  // const [marks, setmarks] = useState([]);
    console.log(data,"data in popup comp")
    
  return (
    <div>
      <Dialog
        open={isModalOpen}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle>  
           <div>
             <div>Name : {data != null ? data.first_name : "--"}</div>
                {data != null ?
                <div>
                    <TableContainer component={Paper}>
      <Table  size="small" aria-label="a dense table">
        <TableHead>
          <TableRow>
            {/* <TableCell align="right">Name</TableCell> */}
            <TableCell align="right">Subjects</TableCell>
            <TableCell align="right">Marks/100</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
        {console.log("data.marks",data.marks)}
        { data.marks != null ? data.subject.map((obj, index) => {
          console.log("object ,,",obj)
              return(
                <>  
                
                <TableRow>
                  <TableCell align="right">{obj}</TableCell>
                  <TableCell align="right">{data.marks[obj]}</TableCell>
                  
                    {/* <TableCell align="right">{data.marks.map((marks, index) => {
                      return(<><span>{marks}</span></>)
                    })}</TableCell>   */}
                
                </TableRow>
                </>
              )
               }): null }
        </TableBody>
      </Table>
    </TableContainer>
                </div>                  
                    //  <div>{data.first_name != null ? data.first_name : "--"}</div> 
                  
                  
            : "no data found"}
           </div>
            
      </DialogTitle>
        <DialogContent>
          <DialogContentText></DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => closeModalPopup()}>OK</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
export default Popup;