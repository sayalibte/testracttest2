import React from 'react';
import { useState, useEffect } from "react";
import "./style.scss";
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import data from "../../studentdata.js";
import Popup from "./Popup.js"
function Student() {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [selected_student,setselected_student]=useState();
  const closeModalPopup = () => {
    setIsModalOpen(false);
  };
  const openModal = (student) => {
    console.log(student,"student open modal check")
    setIsModalOpen(true);
    
    //selected_student = student;
    setselected_student(student);
    
  };
    return (
      <div className="student container">
           {data.map((val, index) => (
          <li key={index}>
            <Grid container spacing={4}>
                  {val.student.map((student, index) => {
                   
                  return(
                    <>
                     <Grid item xs={4}  onClick={() => openModal(student)}>
                         <Paper className="paper">
                            {student.division}  {" "}{" "}{student.roll_number}<br/>
                            {student.first_name} {student.last_name}<br/>
                            {student.bdate}
                         </Paper>
                      </Grid> 
                    </>
                  )
                  })}
            </Grid>
           
          </li>
        ))}
         <Popup
         data={selected_student}
        isModalOpen={isModalOpen}
        closeModalPopup={closeModalPopup}
      />
      </div>
    );
  }
  export default Student;
  