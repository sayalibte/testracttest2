import React from "react";
import { Route } from "react-router-dom";
import Student from "../Components/Student/Index.js";
import Teacher from "../Components/Teacher/Index.js";

export default function AppRoutes() {
  return (
    <div>
      {/* <Route exact path="/home" component={Home} /> */}
      <Route exact path="/student" component={Student} />
      <Route exact path="/teacher" component={Teacher} />
    </div>
  );
}