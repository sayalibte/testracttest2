import React, { useRef, useState, useEffect } from "react";
import './style.scss'
import { Link } from "react-router-dom";
import Paper from '@material-ui/core/Paper';
import MenuItem from '@material-ui/core/MenuItem';
import { AppBar } from "@material-ui/core";
function Nav() {
  
  return (
    <>
      <div className="mainnav">
          {/* <AppBar> */}
      <Paper square className="accountSubmenu submenu">
                            <ul>
                                <div className="menuleft">
                            <MenuItem>
                                    <Link to="/Home">Home</Link>
                                </MenuItem>
                                <MenuItem>
                                    <Link to="/student">Student</Link>
                                </MenuItem>
                                <MenuItem>
                                    <Link to="/teacher">
                                        Teacher
                                    </Link>
                                </MenuItem>
                                </div>
                                <div className="menuright">
                                 <MenuItem style={{align:"right"}}>
                                    <Link to="/">
                                        Notification
                                    </Link>
                                </MenuItem>
                                <MenuItem to="/">
                                <Link to="/">
                                        Log Out
                                    </Link>
                                </MenuItem> 
                                </div>
                            </ul>
                        </Paper>
                        {/* </AppBar> */}
             
      </div>
    </>
  );
}

export default Nav;