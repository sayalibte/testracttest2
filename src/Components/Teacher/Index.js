import React from 'react';
import { useState, useEffect } from "react";
import data from "../../teacherdata.js";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
function Teacher() {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [search, setSearch] = React.useState("");
  const [show, setShowList] = useState(false);
  const [searchData, setSearchData] = useState(data);
  let searched = [];
  //data.filter((dataTeacher,index) => dataTeacher.teacher[index].name =  dataTeacher.teacher[index].first_name.toLowerCase());
  const closeModalPopup = () => {
    setIsModalOpen(false);
  };
  const openModal = (student) => {
    setIsModalOpen(true);
  };
  const handleChange = event => {
    setSearch(event.target.value);
    setShowList(true);
    searchExm();
  };
  let init = 0;
  const searchExm = event =>{
    // data[0].teacher = allData;
    const search = event.target.value;
    console.log("console.l",(!search));
    setSearchData(data) ;
    if(search){
     // console.log("data.teacher >",data)
       data.filter((dataTeacher,index) =>{
        // dataTeacher.teacher.find(element => element.name.includes(search.toLowerCase()));
        if(dataTeacher.first_name.toLowerCase().includes(search.toLowerCase())){
          searched.push(dataTeacher);
        }
        console.log("data >>",dataTeacher.first_name.toLowerCase(),search,dataTeacher.first_name.toLowerCase().includes(search.toLowerCase()));
       
      });
     // data[0].teacher = searched;
      console.log("searched",searched, data[0])
      setSearchData(searched);
    //  }
     // setSearchData(data) ;
     // return data;
    }
    console.log("data >>",data,searchData)

  }

  //console.log("searchData >>", searchData)
  // const displayList = () => {
  //   if (search && searchData.length === 0) {
  //     return <div>no records</div>;
  //   } else if (show && search.length > 0) {
  //     return (
  //       <div>
  //         {searchData.map(listitems => (
  //           <li>{listitems}</li>
  //         ))}
  //       </div>
  //     );
  //   } else {
  //     return null;
  //   }
  // };

  
//   setSearchData= () => !search
//   ? names
//   : data.filter((dataTeacher) =>{
//   console.log("dataTeacher.first_name",dataTeacher.first_name);
//       dataTeacher.first_name.toLowerCase().includes(search.toLowerCase())
// });
   // console.log(searchData,"check nameresult teacher")
    return (
      <div className="teacher">
        <div className="teacher container">
        <input
        type="text"
        placeholder="Search"
       // value={search}
        onChange={searchExm}
      />
           
          <li>
              <TableContainer component={Paper}>
              <Table>
              <TableHead>
                <TableRow>
                <TableCell component="th" scope="row">Teacher ID</TableCell>
                  <TableCell>Name</TableCell>
                  <TableCell>Qualification</TableCell>
                  <TableCell>Date of joining</TableCell>
                  <TableCell>Assign subject</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
              {/* {searchData.map((val, index) => (
                  <> */}
                        {searchData.map((teacher, index) => {   
                        return(
                          <>
                            <TableRow>
                                <TableCell>{teacher.teacher_id}</TableCell>
                                <TableCell>{teacher.first_name}{" "}{teacher.last_name}</TableCell>
                               <TableCell>{teacher.Qualification}</TableCell>
                               <TableCell>{teacher.date_join}</TableCell>
                               <TableCell>{teacher.assign_subject}</TableCell>
                            </TableRow>
                          </> 
                        )
                        })}
                  {/* </>
        
          ))} */}
        </TableBody>
                </Table>
              </TableContainer>
           
          </li>
      
         {/* <Popup
         data={selected_student}
        isModalOpen={isModalOpen}
        closeModalPopup={closeModalPopup}
      /> */}
      </div>
      
      </div>
    );
  }
  export default Teacher;


