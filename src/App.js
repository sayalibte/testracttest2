import React from "react";
import Nav from "./Components/Nav/Index.js";
import Routes from "./Components/Routes";

import { BrowserRouter as Router } from "react-router-dom";
function App() {
  return (
    <div id="app">
      <Router>
        <Nav />
        <Routes />
      </Router>
    </div>
  );
}
export default App;